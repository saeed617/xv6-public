#include "types.h"
#include "user.h"
#include "countTable.h"


int main() {
    char *sys_calls[] = {
            "sys_fork",
            "sys_exit",
            "sys_wait",
            "sys_pipe",
            "sys_read",
            "sys_kill",
            "sys_exec",
            "sys_fstat",
            "sys_chdir",
            "sys_dup",
            "sys_getpid",
            "sys_sbrk",
            "sys_sleep",
            "sys_uptime",
            "sys_open",
            "sys_write",
            "sys_mknod",
            "sys_unlink",
            "sys_link",
            "sys_mkdir",
            "sys_close",
            "sys_counts"
    };
    struct countTable ct;
    int res = counts(&ct);
    if (res < 0) {
        return -1;
    }

    for (int i = 0; i < 22; i++) {
        printf(1, "%s: %d\n", sys_calls[i], ct.sys_call_count[i]);
    }
    exit();
}